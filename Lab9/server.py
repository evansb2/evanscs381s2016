#Bryce Evans
#Work submitted is my own unless otherwise cited
#April 6 2016
import socket
listenSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
listenSocket.bind(('', 12345))
listenSocket.listen(1)
while True:
    tcpSocket,addr = listenSocket.accept()
    print "server connected to " + str(addr)
    receivedMsg = tcpSocket.recv(1024)
    array1 = receivedMsg.partition('http://')
    temp = array1[2]
    array2 = temp.split()
    array3 = array2[0].partition('/')
    host = array3[0]
    filen = array3[1] + array3[2] 
    print "received host: " + str(host)
    print "requested file: " + str(filen)
    tcpSocket.send("You requested " + str(filen) + " from " + str(host))
    tcpSocket.close()

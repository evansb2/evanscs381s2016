# FILL IN HEADER COMMENTS

import socket

MAXFILES = 3 # receive at most this many files
MAXBLOCKS = 1000 # files can't exceed 1000 * 1024 bytes
SERVERTIMEOUT = 30 # after 30 seconds with no requests, shut down server

# Create a server socket that listens for files:
sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
sock.bind(('',12345))
sock.listen(1)
sock.settimeout(SERVERTIMEOUT)
number = 0 # declaring number variable for counting bytes

try: # We handle a server socket timeout at the "except:" line

  for filenum in range(MAXFILES): 
    connection,addr = sock.accept()
    print "DEBUG: connected to" + str(addr)
    connection.settimeout(1)

    # Receive the "PUT" message, extract filename and filesize
    message = connection.recv(1024)
    # split the message to extract filename and filesize
    array = message.split()
    filename = array[1]
    filesize = int(array[2])
    print "DEBUG: received request "+ message
    # Send message
    connection.send('OK')
   

    # Create a new file:
    f = open('_'+filename,'wb')
    
    print "DEBUG: Saving "+filename+" in _"+filename

    
    try:

      # Loop to receive data and send back cumulative bytes received:
        while True: 
            blocks = connection.recv(1024)
            #adding the number each iteration
            number += len(blocks)
            #send number to client
            connection.send(str(number) + ' Bytes' )
            f.write(blocks)
            ##
            if number >= filesize:
                break
        print "DEBUG: Successfully received " + str(filesize) + " Bytes"

      # No more data--close file, send final OK message, and close connection
        f.close()
        connection.send("Ok received " + str(filesize) + " Bytes")
        connection.close()

    except socket.timeout: # This shouldn't happen!
        print "DEBUG: connection timed out (this shouldn't happen!)"
        f.close()
        connection.close()

  sock.close() # Close after MAXFILES files have been received

except socket.timeout:
  print "DEBUG: listener timed out"
  sock.close()

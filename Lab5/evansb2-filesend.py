# FILL IN HEADER COMMENTS 


import socket
import os.path

# Ask user for file name:
filename = raw_input("File to transfer: ")
try:
    # Open the file, get its size
    f = open(filename, 'rb')
    filesize = os.path.getsize(filename)
    # Create socket and connect to the socket created by filerecv.py:
    sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sock.connect(('', 12345))

    # Send the PUT message 
    sock.send("PUT " + filename + " " + str(filesize))
    
    # Receive and print the "OK" message:
    response = sock.recv(1024)
    print response


    # Loop: send the file in blocks of size 1024; after each send,
    # receive a reply from the server and print it.
    while True:
        block = f.read(1024)
        if not block: 
            print "DEBUG: reached end of file"
            break
    ##
        sock.send(block)
      # receive the byte number from server and print 
        byte = sock.recv(1024)
        print byte
    
    # Receive and print final "OK Received...." message, close
    print "DEBUG: closing file "+filename
    lastresponse = sock.recv(1024)
    print lastresponse
    # file, close socket
    f.close()
    sock.close()

except socket.error:
    print "socket error -- can't find server"

except IOError:
    print "no such file"

# Bryce Evans
# All work presented is my own unless otherwise cited
# Lab 3
# 18 February 2016

# We need the time package to calculate round-trip times:
import time 

from socket import *

host = 'localhost'
port = 12345
timeout = 1 # in seconds
 
# Create UDP client socket
# Note the use of SOCK_DGRAM for UDP datagram packet
clientsocket = socket(AF_INET, SOCK_DGRAM)

# Set socket timeout as 1 second
clientsocket.settimeout(timeout)

# Ping 10 times:
# Record variables needed 
number = 0
failures = 0
successes = 0
runtimes = 0
for i in range(1,11):
    number += 1
    message = "Ping %d" %(number) #increment ping numbers
    try:
        print ("Sending: " + message)
	# Save current time (this is the start time):
        startTime = time.time()
	# Send the UDP packet containing the ping message
        clientsocket.sendto(message, (host, port))
	# Receive the server response
        r = clientsocket.recvfrom(1024)
        reply = r[0]
        addr = r[1]
	# Save current time (this is the end time)
        endTime = time.time()
	# Display the server response as an output 
        print "Server reply from", addr, ":" , reply 
	# print round trip time (difference between end time and start time):
        roundTime = endTime - startTime
        runtimes += roundTime
        print "runtimes" , runtimes
        successes += 1 #increment successes
        print "Response Time", roundTime #print round time
        print "**************************************"# help differentiate
       
    except:
        # Server does not respond; assume packet is lost and print message:
        failures += 1 #increment failures
        print 'Request timed out'
        print "*************************************"# help differentiate
        continue

# Close the client socket
# Print failures and successes 
print "Failures %d" %(failures)
print "Success %d" %(successes)
average = runtimes/successes
print "Average RoundTime", average

clientsocket.close()
 

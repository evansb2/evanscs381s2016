# Bryce Evans
# All the work is is my own unless otherwise cited
#
# COMMENT ALL CODE THAT YOU WRITE OR MODIFY.
#


from socket import *

serverSocket = socket(AF_INET, SOCK_STREAM)

serverPort = 12345
serverSocket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
serverSocket.bind(('',serverPort))

serverSocket.listen(1)

while True:
    print '================='
    print 'Ready to serve...'
    print '================='

    connectionSocket, addr = serverSocket.accept()

    try: 
        message = connectionSocket.recv(1024)

        lines = message.split('\n')
        print "Received:"
        print "-----BEGIN-----"
        for line in lines:
          print line
        print "------END------"

        requestline = lines[0].split() # split using whitespace line 1
        requestline2 = lines[1].split() #split using whitespace on line 2

# Verification Test: Not enough fields in first line
        if len(requestline) != 3:
            connectionSocket.send('Error 400 Bad Request: need exactly 3 fields\n')
            
            connectionSocket.close()
            continue
# Verification Test: line 2 has only 2 fields
        if len(requestline2) != 2:
            connectionSocket.send('Error 400 bad Request: need exactly 2 fields\n')
            
            connectionSocket.close()
            continue
# Verification Test: line 1's first word is GET and cannot be misspelled
        if requestline[0] != 'GET':
            connectionSocket.send('Error 400 Bad Request: need GET in first line\n')
            
            connectionSocket.close()
            continue
# Verification Test: line 2's first word is Host:        
        if requestline2[0] != 'Host:':
            connectionSocket.send('Error 400 Bad Request: need Host in second line\n')
            
            connectionSocket.close()
            continue
# Verification Self Made Test: second word is line 2 must be localhost        
        if requestline2[1] != 'localhost':
            connectionSocket.send('Error 400 Bad Request: wrong host header name\n')

            connectionSocket.close()
            continue
        
        #Close client socket


        command = requestline[0]
        filename = requestline[1]
        protocol = requestline[2]


        f = open(filename[1:])
        outputdata = f.read()

        connectionSocket.send('HTTP/1.1 200 OK\n')
        connectionSocket.send('Date: Thu, 04 Jan 2016 14:30:00\n')
        connectionSocket.send('Last-Modified: Thu, 04 Feb 2016 14:40:00\n')
        connectionSocket.send('Content-Type: text/html\n')
        connectionSocket.send('\n')

        for i in range(0, len(outputdata)):
            connectionSocket.send(outputdata[i])

        connectionSocket.close()

    except IOError: 
        
        connectionSocket.send('Error 404 File Not Found\n')

        
        connectionSocket.close()


serverSocket.close()

#Bryce Evans
#April 13, 2016
#All work submitted is my own unless otherwise cited
####################################################
import socket
import time
import os

# Create a "listening socket" that waits for requests:
# Timeout 60 seconds for the listenSocket
listenSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
listenSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
listenSocket.bind(('', 12345)) # runs on localhost
listenSocket.listen(1)
timout = 60
listenSocket.settimeout(timout)

################################################
#new client, client send sthe GET http://hostname/filename HTTP/1.0
#Extract Website hostname and the name of the file
#create the cache name with / -slash-
#print the host name and the file name
##############################################
while True:

  tcpSocket,addr = listenSocket.accept()
  print "Server connected to " +str(addr)

  getRequest = tcpSocket.recv(1024)
  print "Server received " + str(getRequest)

  array1 = getRequest.partition('http://')
  temp = array1[2]
  array2 = temp.split()
  array3 = array2[0].partition('/')
  host = array3[0]
  filen = array3[1] + array3[2]
  cachename = array2[0].replace("/","-slash-")
  
  print "Requested host: " + str(host)  
  print "Requested file: " + str(filen)
  

    
############################################################
  #open the cache file, if cache is true check to see if the 
  #time has expired, if time has expired create new websocket
  # and connect to the host, create new cache file and send 
  #blocks of the file to the client then close the file.
  #if time has not expired send original cache file to the client.
  #if this is a brand new connection create websocket and create
  # new cache file. Send blocks of cache file to client then close
  # the cache file and the tcpsocket

#############################################################

   
  try: 
      cachefile = open(cachename, 'rb')#
      cached = True
      
  except:
      
      cachefile = open(cachename, 'wb')
      cached = False
  if cached:
      current = time.time()
      old = os.path.getmtime(cachename)
      if current-old > 100: 
        cachefile = open(cachename, 'wb')
        webSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM) 
        webSocket.connect((host,80))
        print "Cached version too old -- getting newer version"      
        webSocket.send('GET '+filen+' HTTP/1.0\n\n')
        cachedfile = webSocket.makefile('rb', 0)       
        print "Creating cache file " + cachename
        print "Requesting file " + str(filen) + " from "+ str(host)
        print "Sending " + str(filen) + " to " + str(addr)
        while True:
            block = cachedfile.read(1024)
            if not block:
                break
            tcpSocket.send(block)
            cachefile.write(block)
        cachefile.close()
      else:
        print "Sending cache file " + str(cachename) + " to " + str(addr)

        while True:
            block = cachefile.read(1024)
            if not block: 
                break

            tcpSocket.send(block)
  else:
    webSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    webSocket.connect((host, 80))
    webSocket.send('GET '+filen+' HTTP/1.0\n\n')
    cachedfile = webSocket.makefile('rb', 0)
    print "Creating cache file " + cachename
    print "Requesting file " + str(filen) + " from "+ str(host)
    print "Sending " + str(filen) + " to " + str(addr)
    while True:
        block = cachedfile.read(1024)
        if not block:
            break
        tcpSocket.send(block)
        cachefile.write(block)
    cachefile.close()


  
    tcpSocket.close()
